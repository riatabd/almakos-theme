<li id="post-<?php the_ID(); ?>" class="opinione">
	<a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) { ?>									
			<?php the_post_thumbnail("homepage-slider"); ?>	
		<? } else { ?>

<img width="400" height="280" src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
	</a>
	<div class="autorikategori"><?php $autori = get_cfc_field('autori', 'opinion-nga'); 
						echo $autori;
					?> </div>
	<strong class="opiniondateautori"><?php echo the_time('d.m.Y') ?></strong>
		<?php the_title( sprintf( '<h5><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>' ); ?>
	
</li>