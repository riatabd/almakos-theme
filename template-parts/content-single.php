<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>


<?php twentysixteen_excerpt(); ?>
<?php 
$category = get_the_category();
	$category = $category[0]->category_nicename;

if($category == 'opinione') { ?>

<!-- START OPINIONE -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<header class="entry-header">
		<?php 
				the_title( '<h1 class="entry-title">', '</h1>' ); 
		?>

	</header><!-- .entry-header -->
	<footer class="entry-footer fotoopinione">
	<?php twentysixteen_post_thumbnail(); ?>
	</footer>
	<div class="entry-content">
	<h3>Shkruan: <?php $autori = get_cfc_field('autori', 'opinion-nga'); 
						echo $autori;
					?></h3>
	<div class="addthis_sharing_toolbox entry-header socialsharingikon"></div>
	<div class="kohadatapost">
		 <span style="color: #737070;">Publikuar më <strong style="color: #EC1A23;"><?php echo the_time('d.m.Y') ?></strong></span>
	</div>
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Faqet:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Faqe', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			
		?>
		<div class="addthis_sharing_toolbox"></div>
		
	</div><!-- .entry-content -->
	<footer class="entry-footer">
			<h2 class="temakryesoretitle">OPINIONE TJERA</h2>
			<ul class="temakryesorelist">
					<?php

			$args = array( 'posts_per_page' => 10, 'category' => 8, 'exclude' => get_the_ID() );

			$myposts = get_posts( $args );
			foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

			<li class="autoriopinion" style="padding: 0;">					
				
				<div>
					<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?> <br />
					</a>
				</div>
				<div class="autorilistopinion">
					<?php $autori = get_cfc_field('autori', 'opinion-nga'); 
						echo $autori;
					?>
				</div>
				<div class="titulliopinionit">
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?><br />
					</a>	
				</div>
				

			</li>
			
			<?php endforeach; 
			wp_reset_postdata();?>
			</ul>
	</footer>

</article><!-- #post-## -->

<!-- END OPINIONE -->

<?php
} else {
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		
		<?php 
				the_title( '<h1 class="entry-title">', '</h1>' ); 
		?>

		<div class="kohadatapost">
		<?php $categories = get_the_category();
if ( ! empty( $categories ) ) {
    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
} ?> | <span style="color: #737070;">Publikuar më <strong style="color: #EC1A23;"><?php echo the_time('d.m.Y') ?></strong> në ora <strong style="color: #EC1A23;"><?php echo get_the_time( $format, $post ); ?></strong></span>

<div class="addthis_sharing_toolbox entry-header socialsharingikona"></div>
	</div>

	</header><!-- .entry-header -->
	
	<?php twentysixteen_post_thumbnail(); ?>
	

		<center style="margin-bottom: 30px;"><!-- adxp_almakos_728x90 ROS -->
        <script type='text/javascript' src='http://www.googletagservices.com/tag/js/gpt.js'>
                googletag.pubads().definePassback('4454238/adxp_almakos_728x90', [[728,90]]).display();
        </script></center>

	<div class="entry-content">

		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Faqet:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Faqe', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			
		?>
		<div class="addthis_sharing_toolbox"></div>


<div class="section group">
	<div class="col span_1_of_2">
	<!-- adxp_almakos_300x250_1 ROS -->
        <script type='text/javascript' src='http://www.googletagservices.com/tag/js/gpt.js'>
                googletag.pubads().definePassback('4454238/adxp_almakos_300x250_1', [[300,250]]).display();
        </script>
	</div>
	<div class="col span_1_of_2">
	<script type='text/javascript'>
<!--//<![CDATA[
   document.MAX_ct0 ='';
   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
   var m3_r = Math.floor(Math.random()*99999999999);
   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
   document.write ("zoneid=227310");document.write("&amp;nodis=1");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write ("&amp;loc=" + escape(window.location));
   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
   if (document.context) document.write ("&context=" + escape(document.context));
   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
       document.write ("&amp;ct0=" + escape(document.MAX_ct0));
   }
   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
   document.write ("'></scr"+"ipt>");
//]]>--></script>
	</div>
</div>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
	<!--<h2 style="font-size: .8em;color: #FFB500;">Artikuj të sponsorizuar</h2> -->

		<h2 class="temakryesoretitle">Temat kryesore</h2>
			<ul class="temakryesorelist">
					<?php

			$args = array( 'posts_per_page' => 5, 'category' => 35 );

			$myposts = get_posts( $args );
			foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

			<li>					
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </li>
								
			<?php endforeach; 
			wp_reset_postdata();?>
			</ul>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<?php } ?>