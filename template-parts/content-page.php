<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$slidercategory = 51;
$lajmecategory = 1;
$sportcategory = 15;
$kuriozitetecategory = 11;
$teknologjicategory = 23;
$lifestylecategory = 18;
$ekonomycategory = 12;

?>
<!-- START THE ALMAKOS SLIDER --> 


  <div class="news-holder cf">

    <div class="news-preview">

	<?php

		$args = array( 'posts_per_page' => 1, 'category' => $slidercategory );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

			 <div class="news-content top-content">
			
			<?php if(get_post_format() == 'video') { ?> 
				<span class="sliderartikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="sliderartikujfoto">FOTO</span>
				<?php } ?>
								<?php if ( has_post_thumbnail() ) { ?>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail("homepage-slider"); ?>
								</a>
								<?php } else { ?>
<a href="<?php the_permalink(); ?>">
<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">
</a>
<?php } ?>

						<h2 class="titujkryesore"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

			</div>		
		<?php endforeach; 
		wp_reset_postdata();?>


<?php
		$args = array( 'posts_per_page' => 8, 'category' => $slidercategory, 'offset' => 1 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
			 <div class="news-content">
							
								<?php if ( has_post_thumbnail() ) { ?>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail("homepage-slider"); ?>
								</a>
								<?php } else { ?>
<a href="<?php the_permalink(); ?>">
<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">
</a>
<?php } ?>
						<h2 class="titujkryesore"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

			</div>
		<?php endforeach; 
		wp_reset_postdata();?>


    </div><!-- .news-preview -->

 <ul class="news-headlines">
      <?php

		$args = array( 'posts_per_page' => 1, 'category' => $slidercategory );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<li class="selected"><a href="<?php the_permalink(); ?>" style="color: #000"><?php the_title(); ?></a></li>			
		<?php endforeach; 
		wp_reset_postdata();?>

      <?php

		$args = array( 'posts_per_page' => 8, 'category' => $slidercategory, 'offset' => 1 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
							
								
						<li><a href="<?php the_permalink(); ?>" style="color: #000"><?php the_title(); ?></a></li>
						
							
		<?php endforeach; 
		wp_reset_postdata();?>

      <!-- li.highlight gets inserted here -->
    </ul>
  </div><!-- .news-holder -->

<div id="pl-2">
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $slidercategory, 'offset' => 9 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>
	<?php if(get_post_format() == 'video') { ?> 
	<span class="artikujvideo">VIDEO</span>
	<?php } elseif (get_post_format() == 'image') { ?>
	<span class="artikujfoto">FOTO</span>
	<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>
</div>
<!-- END THE ALMAKOS SLIDER --> 

<!-- LAJME CATEGORY -->
<div id="pl-2">
<div class="lajmeshow">
	<strong class="lajmeemrikategoris">
		LAJME
	</strong>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/maqedoni/">MAQEDONI</a></span> 
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/shqiperi/">SHQIPËRI</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/kosove/">KOSOVA</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/ballkan/">BALLKAN</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/bota/">BOTA</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/komuna/">KOMUNA</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/EKONOMI/">EKONOMI</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/kronike-e-zeze/">KRONIKË E ZEZË</a></span>
    <span class="kategorilisthome"><a href="<?=site_url();?>/kategoria/kulture/">KULTURË</a></span>
    
    <span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/lajme/">më shumë &rsaquo;</a></span>
</div>

	<div class="panel-grid" id="pg-2-0">
		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
			<div class="lajmikryesor">
				<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>
			<?php
					$args = array( 'posts_per_page' => 1, 'category'=> $lajmecategory, 'offset' => 0 );

					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<a href="<?php the_permalink(); ?>">
				
						<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

					<?php if ( has_post_thumbnail() ) { ?>
											
												<?php the_post_thumbnail("large"); ?>
											
											<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
					<h5 class="titujkryesore"><?php the_title(); ?></h5>
					
				
				</a>
				<?php endforeach; 
					wp_reset_postdata();?>
			</div>
		</div>
		<div class="panel-grid-cell" id="pgc-2-0-1">
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 2, 'category'=> $lajmecategory, 'offset' => 1 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>


						<a href="<?php the_permalink(); ?>">
					<li>

					<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

		<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>

				<ul class="lajmetjerashtese">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $lajmecategory, 'offset' => 0 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>
				
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
		</div>

	</div>
	<a href="<?=site_url();?>/kategoria/lajme">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>


<!-- END LAJME CATEGORY -->

<!-- SPORT CATEGORY -->
<div id="pl-2">
<div class="sportshow">
	<strong class="sportemrikategoris">
		SPORT
	</strong>
    
    <span class="sportmeshume"><a href="<?=site_url();?>/kategoria/sport">më shumë &rsaquo;</a></span>
</div>

	<div class="panel-grid" id="pg-2-0">
		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
			<div class="lajmikryesor">
			<?php
					$args = array( 'posts_per_page' => 1, 'category'=> $sportcategory, 'offset' => 0 );

					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<a href="<?php the_permalink(); ?>">
						
						<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

					<?php if ( has_post_thumbnail() ) { ?>
											
												<?php the_post_thumbnail("large"); ?>
											
											<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
					<h5 class="titujkryesore"><?php the_title(); ?></h5>
					
				
					</a>
				<?php endforeach; 
					wp_reset_postdata();?>
			</div>
		</div>
		<div class="panel-grid-cell" id="pgc-2-0-1">
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $sportcategory, 'offset' => 1 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>
					<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>
						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>

						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
				
		</div>
	</div>
	<a href="<?=site_url();?>/kategoria/sport">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>

<!-- END SPORT CATEGORY -->

<!-- SHOWBIZ CATEGORY -->
<div id="pl-2">
<div class="showbizshow">
	<strong class="showbizemrikategoris">
		SHOWBIZ
	</strong>
    
    <span class="showbizmeshume"><a href="<?=site_url();?>/kategoria/showbiz">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> 9, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>
	<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>

<a href="<?=site_url();?>/kategoria/showbiz">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
<!-- END SHOWBIZ CATEGORY -->

<!-- KURIOZITETE & TEKNOLOGJI CATEGORY -->
<div id="pl-2">
	<div class="panel-grid" id="pg-2-0">
		<!-- KURIOZITETE -->
		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
		<div class="kurioziteteshow">
			<strong class="kurioziteteemrikategoris">
				KURIOZITETE
			</strong>
    
    		<span class="kuriozitetemeshume"><a href="<?=site_url();?>/kategoria/kuriozitete">më shumë &rsaquo;</a></span>
		</div>
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $kuriozitetecategory, 'offset' => 0 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>
					<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
				<a href="<?=site_url();?>/kategoria/kuriozitete">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
		</div>
		<!-- END KURIOZITETE -->

		<!-- TEKNOLOGJI -->
		<div class="panel-grid-cell" id="pgc-2-0-1">
			<div class="teknologjishow">
				<strong class="teknologjiemrikategoris">
					AUTO & TECH
				</strong>
			    
			    <span class="teknologjimeshume"><a href="<?=site_url();?>/kategoria/teknologji">më shumë &rsaquo;</a></span>
			</div>
			<ul class="rig columns-3">
				<?php
						$args = array( 'posts_per_page' => 6, 'category'=> $teknologjicategory, 'offset' => 0 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>
					<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
			
		</div>
		<!-- END TEKNOLOGJI -->
	</div>
		<a href="<?=site_url();?>/kategoria/teknologji">
					<div class="lexomeshume">
						MË SHUMË <span>&rsaquo;</span>
					</div>
				</a>
</div>

<!-- END KURIOZITETE & TEKNOLOGJI CATEGORY -->


<!-- LIFESTYLE CATEGORY -->
<div id="pl-2">
<div class="lifestyleshow">
	<strong class="lifestyleemrikategoris">
		LIFESTYLE
	</strong>
    
    <span class="lifestylemeshume"><a href="kategoria/lifestyle">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $lifestylecategory, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>
	<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>

<a href="/kategoria/lifestyle">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
<!-- END LIFESTYLE CATEGORY -->



<!-- EKONOMI CATEGORY -->
<div id="pl-2">
<div class="ekonomishow">
	<strong class="ekonomiemrikategoris">
		EKONOMI
	</strong>
    
    <span class="ekonomimeshume"><a href="kategoria/ekonomi">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $ekonomycategory, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>

	<?php if(get_post_format() == 'video') { ?> 
				<span class="artikujvideo">VIDEO</span>
				<?php } elseif (get_post_format() == 'image') { ?>
				<span class="artikujfoto">FOTO</span>
				<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>

<a href="/kategoria/ekonomi">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
<!-- END EKONOMI CATEGORY -->