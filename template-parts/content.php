<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<li id="post-<?php the_ID(); ?>">

<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>

	<a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) { ?>									
			<?php the_post_thumbnail("homepage-slider"); ?>	
		<?php } else { ?>

<img width="400" height="280" src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
	</a>
	<div class="blocktitulli">
	<div class="kohadatapost">
		<strong style="color: #EC1A23;"><?php echo the_time('d.m.Y') ?></strong> | 
		<strong style="color: #EC1A23;"><?php echo get_the_time( $format, $post ); ?></strong>
	</div>
		<?php the_title( sprintf( '<h5 class="brendakategoritituj"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>' ); ?>
	</div>
</li>
