<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$slidercategory = 36;
$lajmecategory = 1;
$sportcategory = 15;
$kuriozitetecategory = 11;
$teknologjicategory = 23;
$lifestylecategory = 18;
$ekonomycategory = 12;
$kulturecategory = 14;
$kronikecategory = 13;
$komunacategory = 50;
$botacategory = 4;
$shqiperiacategory = 7;
$maqedonicategory = 52;
$kosovacategory = 6;
$ballkancategory = 53;

get_header();
?>
<style type="text/css" media="all" id="siteorigin-panels-grids-wp_head">/* Layout 2 */ #pgc-2-0-0 , #pgc-2-0-1 { width:50% } #pg-2-0 .panel-grid-cell { float:left } #pl-2 .panel-grid-cell .so-panel { margin-bottom:30px } #pl-2 .panel-grid-cell .so-panel:last-child { margin-bottom:0px } #pg-2-0 { margin-left:-15px;margin-right:-15px } #pg-2-0 .panel-grid-cell { padding-left:15px;padding-right:15px } @media (max-width:780px){ #pg-2-0 .panel-grid-cell { float:none;width:auto } #pgc-2-0-0 { margin-bottom:30px } #pl-2 .panel-grid { margin-left:0;margin-right:0 } #pl-2 .panel-grid-cell { padding:0 }  } </style>
<!-- START THE ALMAKOS SLIDER --> 

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">



<!-- LAJME CATEGORY -->
<div class="rreshtakategori">
<div id="pl-2">

	<div class="panel-grid" id="pg-2-0">
		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
			<div class="lajmikryesor">

			<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>

			<?php
					$args = array( 'posts_per_page' => 1, 'category'=> $lajmecategory, 'offset' => 0 );

					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<a href="<?php the_permalink(); ?>">
				
					<?php if ( has_post_thumbnail() ) { ?>
											
												<?php the_post_thumbnail("large"); ?>
											
											<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
					<h5 class="titujkryesore"><?php the_title(); ?></h5>
					
				
				</a>
				<?php endforeach; 
					wp_reset_postdata();?>
			</div>

		</div>


		<div class="panel-grid-cell" id="pgc-2-0-1">
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $lajmecategory, 'offset' => 1 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>

					<?php if(get_post_format() == 'video') { ?> 
					<span class="artikujvideo">VIDEO</span>
					<?php } elseif (get_post_format() == 'image') { ?>
					<span class="artikujfoto">FOTO</span>
					<?php } ?>

						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>

		</div>

	</div>
	</div>

<div id="pl-2">

<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $lajmecategory, 'offset' => 9 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>
	<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>
<a href="<?=site_url();?>/kategoria/lajme">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
<!-- END THE ALMAKOS SLIDER --> 

	

</div>

<!-- END LAJME CATEGORY -->

<!-- MAQEDONI CATEGORY -->
<div id="pl-2">
<div class="rreshtakategori">
<div class="lajmeshow">
	<strong class="lajmeemrikategoris">
		MAQEDONI
	</strong>
    
    <span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/lajme/maqedoni">më shumë &rsaquo;</a></span>
</div>

	<div class="panel-grid" id="pg-2-0">
		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
			<div class="lajmikryesor">
			<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
			<?php
					$args = array( 'posts_per_page' => 1, 'category'=> $maqedonicategory, 'offset' => 0 );

					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<a href="<?php the_permalink(); ?>">
				
					<?php if ( has_post_thumbnail() ) { ?>
											
												<?php the_post_thumbnail("large"); ?>
											
											<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
					<h5 class="titujkryesore"><?php the_title(); ?></h5>
					
				
					</a>
				<?php endforeach; 
					wp_reset_postdata();?>
			</div>
		</div>
		<div class="panel-grid-cell" id="pgc-2-0-1">
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $maqedonicategory, 'offset' => 1 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>
					<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>

						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
				
		</div>
	</div>
	<a href="<?=site_url();?>/kategoria/sport">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
</div>
<!-- END MAQEDONI CATEGORY -->

<!-- KOSOVA CATEGORY -->
<div id="pl-2">
<div class="rreshtakategori">
<div class="lajmeshow">
	<strong class="lajmeemrikategoris">
		KOSOVA
	</strong>
    
    <span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/lajme/kosove">më shumë &rsaquo;</a></span>
</div>

	<div class="panel-grid" id="pg-2-0">
		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
			<div class="lajmikryesor">

			<?php
					$args = array( 'posts_per_page' => 1, 'category'=> $kosovacategory, 'offset' => 0 );

					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<a href="<?php the_permalink(); ?>">

					<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
				
					<?php if ( has_post_thumbnail() ) { ?>
											
												<?php the_post_thumbnail("large"); ?>
											
											<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/400-280-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
					<h5 class="titujkryesore"><?php the_title(); ?></h5>
					
				
					</a>
				<?php endforeach; 
					wp_reset_postdata();?>
			</div>
		</div>
		<div class="panel-grid-cell" id="pgc-2-0-1">
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $kosovacategory, 'offset' => 1 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>

					<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>

						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
				
		</div>
	</div>
	<a href="<?=site_url();?>/kategoria/sport">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
</div>
<!-- END KOSOVA CATEGORY -->

<!-- SHQIPERIA CATEGORY -->
<div id="pl-2">
<div class="rreshtakategori">
<div class="lajmeshow">
	<strong class="lajmeemrikategoris">
		SHQIPËRI
	</strong>
    
    <span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/lajme/shqiperi">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $showbizcategory, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>

	<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>

<a href="<?=site_url();?>/kategoria/showbiz">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
</div>
<!-- END SHQIPERIA CATEGORY -->


<!-- BOTA CATEGORY -->
<div id="pl-2">
<div class="rreshtakategori">
<div class="lajmeshow">
	<strong class="lajmeemrikategoris">
		BOTA
	</strong>
    
    <span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/lajme/bota">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 8, 'category'=> $botacategory, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>

	<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>

<a href="<?=site_url();?>/kategoria/showbiz">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
</div>
<!-- END BOTA CATEGORY -->


<!-- BALLKAN & KOMUNA -->
<div id="pl-2">
<div class="rreshtakategori">

	<div class="panel-grid" id="pg-2-0">

		<div class="panel-grid-cell" id="pgc-2-0-0" style="padding-right: 0;">
		<div class="lajmeshow">
			<strong class="lajmeemrikategoris">
				BALLKAN
			</strong>
    
    		<span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/kuriozitete">më shumë &rsaquo;</a></span>
		</div>
			<ul class="rig columns-2">
				<?php
						$args = array( 'posts_per_page' => 4, 'category'=> $ballkancategory, 'offset' => 0 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>

					<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
				<a href="<?=site_url();?>/kategoria/kuriozitete">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
		</div>
		<!-- END BALLKAN -->

		<!-- KOMUNA -->
		<div class="panel-grid-cell" id="pgc-2-0-1">
			<div class="lajmeshow">
				<strong class="lajmeemrikategoris">
					KOMUNA
				</strong>
			    
			    <span class="lajmemeshume"><a href="<?=site_url();?>/kategoria/komuna">më shumë &rsaquo;</a></span>
			</div>
			<ul class="rig columns-3">
				<?php
						$args = array( 'posts_per_page' => 6, 'category'=> $komunacategory, 'offset' => 0 );

						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>">
					<li>

					<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>
						<?php if ( has_post_thumbnail() ) { ?>
												
													<?php the_post_thumbnail("homepage-thumb"); ?>
												
												<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
						<h5><?php the_title(); ?></h5>
						
					</li>
					</a>
					<?php endforeach; 
						wp_reset_postdata();?>
				</ul>
				<a href="<?=site_url();?>/kategoria/teknologji">
					<div class="lexomeshume">
						MË SHUMË <span>&rsaquo;</span>
					</div>
				</a>
		</div>
		<!-- END KOMUNA -->
	</div>
</div>
</div>
<!-- END BOTA & KOMUNA CATEGORY -->


<!-- KRONIKË E ZEZË CATEGORY -->
<div id="pl-2">
<div class="rreshtakategori">
<div class="kronikshow">
	<strong class="kronikemrikategoris">
		KRONIKË E ZEZË
	</strong>
    
    <span class="kronikmeshume"><a href="kategoria/kronike-e-zeze">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $kronikecategory, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>

	<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>
<a href="/kategoria/lifestyle">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
</div>
<!-- END KRONIKË E ZEZË CATEGORY -->

<!-- KULTURA CATEGORY -->
<div id="pl-2">
<div class="rreshtakategori">
<div class="kurioziteteshow">
	<strong class="kurioziteteemrikategoris">
		KULTURA
	</strong>
    
    <span class="kuriozitetemeshume"><a href="kategoria/kulture">më shumë &rsaquo;</a></span>
</div>
<ul class="rig columns-4">
<?php

		$args = array( 'posts_per_page' => 4, 'category'=> $kulturecategory, 'offset' => 0 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<a href="<?php the_permalink(); ?>">
	<li>

	<?php if(get_post_format() == 'video') { ?> 
<span class="artikujvideo">VIDEO</span>
<?php } elseif (get_post_format() == 'image') { ?>
<span class="artikujfoto">FOTO</span>
<?php } ?>

		<?php if ( has_post_thumbnail() ) { ?>
	
									<?php the_post_thumbnail("homepage-thumb"); ?>
				
								<?php } else { ?>

<img src="<?php bloginfo('template_directory'); ?>/img/200-112-img.jpg" class="attachment-homepage-slider size-homepage-slider wp-post-image" alt="">

<?php } ?>
		<h5><?php the_title(); ?></h5>
	</li>
	</a>
	<?php endforeach; 
		wp_reset_postdata();?>
</ul>
<a href="/kategoria/lifestyle">
		<div class="lexomeshume">
			MË SHUMË <span>&rsaquo;</span>
		</div>
	</a>
</div>
</div>
<!-- END KULTURA CATEGORY -->

</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
