<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			
			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'social',
							'menu_class'     => 'social-links-menu',
							'depth'          => 1,
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>',
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>

			<div class="section group">
					<div class="col span_1_of_3">
					<img src="<?php echo site_url(); ?>/wp-content/uploads/2016/05/cropped-logo.png" style="margin-bottom: 15px;"><br />
					<strong style="font-size: 1.3em">ALMAKOS MEDIA GROUP</strong><br />
					<div class="infoalmakos">
						<strong>Adresa:</strong>  Ul.Stole Naumov bb, Ndërtesa 4 kati 3<br />1000 Shkup / Maqedoni<br />
						<strong>Tel/fax:</strong> 02/3296-258 & 02/3296-259,<br />
						<strong>Email:</strong> info@almakos.com & almakoss@gmail.com<br />
					</div>
				
					</div>
					<div class="col span_2_of_3">
						<?php
							wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'menuposht',
							'depth' => 1,
						 	) );
						?>
						<div class="tedrejtat">
						Portali është në pronësi të Almakos, lajmet dhe informacionet tjera nga portali nuk mund të kopjohen, të printohen, ose të përdoren në çfarëdo forme tjetër për qëllime përfitimi. Për të shfrytëzuar informatat e këtij portali ju lutemi kontaktoni personat përgjegjës, ose shkruani në email-in oficial të portalit.
						</div>

						<div class="dizajnimi">Zhvillimi dhe mirëmbajtja: <a href="http://www.gidasolution.com/" target="_blank">GIDA SOLUTION</a></div>
					</div>
				</div>

		

		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
 <?php if (!is_front_page()) { ?>
 <!-- Go to www.addthis.com/dashboard to customize your tools -->
 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-573ec846f09b33b0"></script>
<?php
 }
 ?>
 
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri() ?>/js/vertical.news.slider.js"></script>

</body>
</html>
